# Audio setup on raspberry pi

Make sure that hdmi is on, if that's what you wanna use, by getting its status
with

```
sudo tvservice -s
```

and turning it on if it's off by running it with preferred settings like such:

```
sudo tvservice -p
```

Now, to make sure that alsa (which is the only thing there, a priori, I think),
run for instance

```
aplay -l
```

to list (I think) sound cards.

if you something rather than nothing there, you're good.

Now get your hands on some audio file, and play it for instance using

```
sudo aplay -t [sound_type] [sound_file]
```

to check on your volume, use

```
sudo amixer scontents
```

in the last line, there's a percentage - that's your volume!

to adjust it, use e.g.

```
amixer set 'PCM' 60%
```

and try again.

then we need to setup pulseaudio, to run a server accessible from other
machines on the network.

install the following packages:

```
sudo apt-get install -y pulseaudio pulseaudio-esound-compat \
  pulseaudio-utils pulseaudio-module-zeroconf
```

Finally, edit `/etc/pulse/default.pa` and add (or replace the commented
versions of) the following two lines:

```
load-module module-native-protocol-tcp auth-ip-acl=127.0.0.1;192.168.0.0/24 auth-anonymous=1
load-module module-zeroconf-publish
```

where you should of course replace `192.168.0.0/24` with your local ip subnet
that you want to trust for anonymous connecting to your pulse server.

the zeroconf publish makes sure that clients can discover your server without
further ado, or automagically. for that to work you have to do something on the
client side as well, namely adding

```
load-module module-zeroconf-discover
```

to `/etc/pulse/default.pa`

afterwards, you may use `pavucontrol` to set volume, choose a sink, and pick it
as a fall-back on the client.

To actually start the pulse server, run

```
pulseaudio --start
```

where the realtime and highpriority options are efforts to make a stutter
disappear. doesn't help too much though : (

make sure that you added your user to all the relevant pulse groups, or run as
sudo.
